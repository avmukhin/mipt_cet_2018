#include "pch.h"
#include "Complex.h"

TEST(complex, Plus) {
	Complex c1(0, 1), c2(1,0), c(1,1);
	EXPECT_EQ(c1+c2, c);
}

TEST(complex, Minus) {
	Complex c1(0, 1), c2(1, 0), c(-1, 1);
	EXPECT_EQ(c1 - c2, c);
}

TEST(complex, Multiply) {
	Complex c1(0, 1), c2(1, 0), c(0, 1);
	EXPECT_EQ(c1 * c2, c);
}

TEST(complex, Frac) {
	Complex c1(0, 1), c2(1, 0), c(0, 1);
	EXPECT_EQ(c1 / c2, c);
}

TEST(complex, Abs) {
	Complex c1(0, 1), c2(1, 0), c(0, 1);
	EXPECT_EQ(c2.abs(), 1);
}

TEST(complex, Inv) {
	Complex c1(0, 1), c2(1, 0), c(0, -1);
	EXPECT_EQ(~c1, c);
}