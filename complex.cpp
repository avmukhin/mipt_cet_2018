#include <iostream>
#include <string>
#include <math.h>

class Complex {
private:
	double real;
	double image;

public:
	Complex(double real, double image) {
		this->real = real;
		this->image = image;
	}

	Complex() {
		this->real = 0;
		this->image = 0;

	}


	double getReal() {
		return real;
	}

	double getImg() {
		return image;
	}

	Complex operator+(Complex x) {
		double rsum = real + x.getReal();
		double isum = image + x.getImg();
		return Complex(rsum, isum);
	}

	Complex operator-(Complex x) {
		double rsum = real - x.getReal();
		double isum = image - x.getImg();
		return Complex(rsum, isum);
	}

	double abs() {
		double sabs = pow(pow(real, 2) + pow(image, 2), 0.5);
		return sabs;
	}

	Complex operator*(Complex x) {
		double rmul = real * x.getReal() - image * x.getImg();
		double imul = real * x.getImg() + image * x.getReal();
		return Complex(rmul, imul);
	}

	Complex operator/(Complex x) {
		double rdiv = (real * x.getReal() + image * x.getImg()) / (pow(x.getReal(), 2) + pow(x.getImg(), 2));
		double idiv = (x.getReal() * image - real * x.getImg()) / (pow(x.getReal(), 2) + pow(x.getImg(), 2));
		return Complex(rdiv, idiv);
	}

	Complex operator~() {
		return Complex(real, -image);
	}


};

std::ostream& operator<<(std::ostream& cout, Complex Numba) {
	if (Numba.getImg() < 0)
		cout << "(" << Numba.getReal() << " " << Numba.getImg() << "i" << ")";
	else
		cout << "(" << Numba.getReal() << " " << "+" << " " << Numba.getImg() << "i" << ")";
	return cout;
}

int main()
{
	Complex a(5, -5);
	Complex b(1, -1);

	std::cout << a << " + " << b << " = " << a + b << std::endl;
	std::cout << std::endl;
	std::cout << a << " - " << b << " = " << a - b << std::endl;
	std::cout << std::endl;
	std::cout << a << " * " << b << " = " << a * b << std::endl;
	std::cout << std::endl;
	std::cout << a << " / " << b << " = " << a / b << std::endl;
	std::cout << std::endl;

	
	std::cout << "abs(" << a << ") = " << a.abs() << std::endl;
	std::cout << std::endl;
	std::cout << "~(" << b << ") = " << ~b << std::endl;
	std::cout << std::endl;
	//std::cout << A << std::endl;
	//	std::cout << B.getReal();
	system("pause");
	return 0;
}
